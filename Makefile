##############################################################################
# General targets
##############################################################################

python = python3
venv = venv

.PHONY: venv
venv: venv-create venv-deps

.PHONY: venv-create
venv-create:
	$(python) -m venv --clear $(venv)

.PHONY: venv-deps
venv-deps:
	$(venv)/bin/pip install --upgrade pip
	$(venv)/bin/pip install --editable .
	$(venv)/bin/pip install --requirement requirements-dev.txt
	$(venv)/bin/pip install --requirement requirements-docs.txt

.PHONY: test-all
test-all: style lint check test

.PHONY: style
style:
	$(venv)/bin/pycodestyle hangups

.PHONY: lint
lint:
	$(venv)/bin/pylint --reports=n hangups

.PHONY: check
check:
	$(venv)/bin/python setup.py check --metadata --restructuredtext --strict

.PHONY: test
test:
	$(venv)/bin/pytest hangups

.PHONY: docs
docs:
	. $(venv)/bin/activate && make -C docs html

.PHONY: clean
clean:
	rm -rf $(venv) `find . -name __pycache__`

##############################################################################
# Protocol buffer targets
##############################################################################

protoc = protoc
proto_doc = docs/proto.rst

protobufs = hangups/hangouts.proto hangups/googlechat.proto hangups/test/test_pblite.proto
py_protobufs = $(protobufs:%.proto=%_pb2.py)

%_pb2.py: %.proto
	$(protoc) --python_out . $<

.PHONY: protos
protos: $(py_protobufs) $(proto_doc)

.PHONY: clean-protos
clean-protos:
	rm -f $(py_protobufs) $(proto_doc)

$(proto_doc): hangups/googlechat.proto
	$(venv)/bin/python docs/generate_proto_docs.py $< > $@
